<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Aplicacion numero 2 - Ejemplo 2 - Utilizacion de Yii</h1>
    <br>
</p>

Vamos a crear una aplicacion para entender la forma de utilizar las rutas en Yii.
Ademas vamos a utilizar el controlador para poder enviar a las vistas datos.
En las vistas vamos a utilizar Helpers para las etiquetas HTML

## INSTALACION

Para poder instalar el esqueleto original basico de una aplicacion en Yii

### Utilizando composer

~~~
composer create-project --prefer-dist yiisoft/yii2-app-basic aplicacion2
~~~

Si tuvieras la carpeta aplicacion2 creada la instruccion seria

~~~
composer create-project --prefer-dist yiisoft/yii2-app-basic .
~~~

Para poder comprobar el funcionamiento de la aplicacion recien descargada debes reiniciar Laragon para la gestion del fichero HOST y despues escribir

~~~
http://aplicacion2.test
~~~

##CONFIGURACION

Para realizar las configuraciones basicas de la aplicacion debemos acceder a la carpeta config.

### Archivo web.php

Para las configuraciones del nombre de la aplicacion, el id y el idioma

~~~
$config = [
    'id' => 'aplicacion2', // colocar el id de la aplicacion
    'name' => 'Aplicacion 2 de Yii - Ejemplo 2', // esto es el nombre de la aplicacion
    // el nombre de la aplicacion sale por defecto en la barra de menus
    'language' => 'es_es', // coloco el idioma de la aplicacion en español de españa
    'basePath' => dirname(__DIR__),

 ~~~

Para configurar las rutas limpias y que no coloque el index.php en las rutas
 ~~~
    // activo la gestion de rutas de la aplicacion de forma limpia
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
~~~

## APLICACION

La aplicacion solicitada es
